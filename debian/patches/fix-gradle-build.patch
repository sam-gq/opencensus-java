Description: Allow Gradle to correctly build this package
Author: Olek Wojnar <olek@debian.org>
Forwarded: not-needed
Last-Update: 2020-06-15

--- a/build.gradle
+++ b/build.gradle
@@ -8,7 +8,6 @@
     }
     dependencies {
         classpath 'ru.vyarus:gradle-animalsniffer-plugin:1.4.6'
-        classpath("org.springframework.boot:spring-boot-gradle-plugin:2.0.5.RELEASE")
         classpath 'net.ltgt.gradle:gradle-errorprone-plugin:0.0.16'
         classpath "net.ltgt.gradle:gradle-apt-plugin:0.18"
         classpath 'com.github.ben-manes:gradle-versions-plugin:0.20.0'
@@ -20,7 +19,6 @@
 
 // Display the version report using: ./gradlew dependencyUpdates
 // Also see https://github.com/ben-manes/gradle-versions-plugin.
-apply plugin: 'com.github.ben-manes.versions'
 
 subprojects {
     // OC-Agent exporter depends on grpc-core which depends on errorprone annotations, and it
@@ -28,8 +26,8 @@
     def projectsDependOnGrpcCore = ["opencensus-exporter-metrics-ocagent", "opencensus-exporter-trace-ocagent"]
 
     // Don't use the Checker Framework by default, since it interferes with Error Prone.
-    def useCheckerFramework = rootProject.hasProperty('checkerFramework') && !(project.name in projectsDependOnGrpcCore)
-    def useErrorProne = !useCheckerFramework
+    def useCheckerFramework = false
+    def useErrorProne = false
 
     apply plugin: "checkstyle"
     apply plugin: 'maven'
@@ -38,17 +36,8 @@
     apply plugin: 'java'
     apply plugin: "signing"
     apply plugin: "jacoco"
-    // The plugin only has an effect if a signature is specified
-    apply plugin: 'ru.vyarus.animalsniffer'
     apply plugin: 'findbugs'
     apply plugin: 'net.ltgt.apt'
-    apply plugin: 'net.ltgt.apt-idea'
-    apply plugin: "me.champeau.gradle.jmh"
-    apply plugin: "io.morethan.jmhreport"
-    apply plugin: 'com.github.sherter.google-java-format'
-    if (useErrorProne) {
-        apply plugin: "net.ltgt.errorprone"
-    }
 
     group = "io.opencensus"
     version = "0.24.0" // CURRENT_OPENCENSUS_VERSION
@@ -72,7 +61,7 @@
         }
     }
 
-    [compileJava, compileTestJava, compileJmhJava].each() {
+    [compileJava, compileTestJava].each() {
         // We suppress the "try" warning because it disallows managing an auto-closeable with
         // try-with-resources without referencing the auto-closeable within the try block.
         // We suppress the "processing" warning as suggested in
@@ -155,7 +144,6 @@
     }
 
     ext {
-        appengineVersion = '1.9.71'
         aspectjVersion = '1.8.11'
         autoValueVersion = '1.4'
         findBugsAnnotationsVersion = '3.0.1'
@@ -185,9 +173,9 @@
         httpcomponentsVersion = "4.5.8"
 
         libraries = [
-                appengine_api: "com.google.appengine:appengine-api-1.0-sdk:${appengineVersion}",
                 aspectj: "org.aspectj:aspectjrt:${aspectjVersion}",
                 auto_value: "com.google.auto.value:auto-value:${autoValueVersion}",
+                auto_value_a: "com.google.auto.value:auto-value-annotations:${autoValueVersion}",
                 auto_service: 'com.google.auto.service:auto-service:1.0-rc3',
                 byte_buddy: 'net.bytebuddy:byte-buddy:1.8.22',
                 config: 'com.typesafe:config:1.2.1',
@@ -304,12 +292,6 @@
             html.enabled = true
         }
     }
-    findbugsJmh {
-        reports {
-            xml.enabled = false
-            html.enabled = true
-        }
-    }
 
     checkstyle {
         configFile = file("$rootDir/buildscripts/checkstyle.xml")
@@ -321,22 +303,6 @@
         configProperties["rootDir"] = rootDir
     }
 
-    googleJavaFormat {
-        toolVersion '1.7'
-    }
-
-    afterEvaluate {  // Allow subproject to add more source sets.
-        tasks.googleJavaFormat {
-            source = sourceSets*.allJava
-            include '**/*.java'
-        }
-
-        tasks.verifyGoogleJavaFormat {
-            source = sourceSets*.allJava
-            include '**/*.java'
-        }
-    }
-
     signing {
         required false
         sign configurations.archives
@@ -356,29 +322,6 @@
         archives javadocJar, sourcesJar
     }
 
-    jmh {
-        jmhVersion = '1.20'
-        warmupIterations = 10
-        iterations = 10
-        fork = 1
-        failOnError = true
-        resultFormat = 'JSON'
-        // Allow to run single benchmark class like:
-        // ./gradlew -PjmhIncludeSingleClass=StatsTraceContextBenchmark clean :grpc-core:jmh
-        if (project.hasProperty('jmhIncludeSingleClass')) {
-            include = [
-                    project.property('jmhIncludeSingleClass')
-            ]
-        }
-    }
-
-    jmhReport {
-        jmhResultPath = project.file("${project.buildDir}/reports/jmh/results.json")
-        jmhReportOutput = project.file("${project.buildDir}/reports/jmh")
-    }
-
-    tasks.jmh.finalizedBy tasks.jmhReport
-
     uploadArchives {
         repositories {
             mavenDeployer {
@@ -433,7 +376,6 @@
     uploadArchives.onlyIf {
         name in ['opencensus-api',
                  'opencensus-contrib-agent',
-                 'opencensus-contrib-appengine-standard-util',
                  'opencensus-contrib-dropwizard',
                  'opencensus-contrib-dropwizard5',
                  'opencensus-contrib-exemplar-util',
--- a/settings.gradle
+++ b/settings.gradle
@@ -1,97 +1,9 @@
 rootProject.name = "opencensus-java"
 
-include ":opencensus-all"
 include ":opencensus-api"
-include ":opencensus-benchmarks"
-include ":opencensus-impl-core"
-include ":opencensus-impl-lite"
-include ":opencensus-impl"
-include ":opencensus-testing"
-include ":opencensus-exporter-metrics-ocagent"
-include ":opencensus-exporter-metrics-util"
-include ":opencensus-exporter-trace-datadog"
-include ":opencensus-exporter-trace-elasticsearch"
-include ":opencensus-exporter-trace-instana"
-include ":opencensus-exporter-trace-logging"
-include ":opencensus-exporter-trace-ocagent"
-include ":opencensus-exporter-trace-stackdriver"
-include ":opencensus-exporter-trace-zipkin"
-include ":opencensus-exporter-trace-jaeger"
-include ":opencensus-exporter-trace-util"
-include ":opencensus-exporter-stats-signalfx"
-include ":opencensus-exporter-stats-stackdriver"
-include ":opencensus-exporter-stats-prometheus"
-include ":opencensus-contrib-agent"
-include ":opencensus-contrib-appengine-standard-util"
-include ":opencensus-contrib-dropwizard"
-include ":opencensus-contrib-dropwizard5"
-include ":opencensus-contrib-exemplar-util"
 include ":opencensus-contrib-grpc-metrics"
-include ":opencensus-contrib-grpc-util"
-include ":opencensus-contrib-http-jaxrs"
-include ":opencensus-contrib-http-jetty-client"
-include ":opencensus-contrib-http-servlet"
 include ":opencensus-contrib-http-util"
-include ":opencensus-contrib-log-correlation-log4j2"
-include ":opencensus-contrib-log-correlation-stackdriver"
-include ":opencensus-contrib-resource-util"
-include ":opencensus-contrib-spring"
-include ":opencensus-contrib-spring-sleuth-v1x"
-include ":opencensus-contrib-spring-starter"
-include ":opencensus-contrib-zpages"
 
-project(':opencensus-all').projectDir = "$rootDir/all" as File
 project(':opencensus-api').projectDir = "$rootDir/api" as File
-project(':opencensus-benchmarks').projectDir = "$rootDir/benchmarks" as File
-project(':opencensus-impl-core').projectDir = "$rootDir/impl_core" as File
-project(':opencensus-impl-lite').projectDir = "$rootDir/impl_lite" as File
-project(':opencensus-impl').projectDir = "$rootDir/impl" as File
-project(':opencensus-testing').projectDir = "$rootDir/testing" as File
-project(':opencensus-contrib-agent').projectDir = "$rootDir/contrib/agent" as File
-project(':opencensus-contrib-appengine-standard-util').projectDir =
-        "$rootDir/contrib/appengine_standard_util" as File
-project(':opencensus-contrib-dropwizard').projectDir = "$rootDir/contrib/dropwizard" as File
-project(':opencensus-contrib-dropwizard5').projectDir = "$rootDir/contrib/dropwizard5" as File
-project(':opencensus-contrib-exemplar-util').projectDir = "$rootDir/contrib/exemplar_util" as File
 project(':opencensus-contrib-grpc-metrics').projectDir = "$rootDir/contrib/grpc_metrics" as File
-project(':opencensus-contrib-grpc-util').projectDir = "$rootDir/contrib/grpc_util" as File
-project(':opencensus-contrib-http-jaxrs').projectDir = "$rootDir/contrib/http_jaxrs" as File
-project(':opencensus-contrib-http-jetty-client').projectDir =
-        "$rootDir/contrib/http_jetty_client" as File
-project(':opencensus-contrib-http-servlet').projectDir = "$rootDir/contrib/http_servlet" as File
 project(':opencensus-contrib-http-util').projectDir = "$rootDir/contrib/http_util" as File
-project(':opencensus-contrib-log-correlation-log4j2').projectDir =
-        "$rootDir/contrib/log_correlation/log4j2" as File
-project(':opencensus-contrib-log-correlation-stackdriver').projectDir =
-        "$rootDir/contrib/log_correlation/stackdriver" as File
-project(':opencensus-contrib-resource-util').projectDir = "$rootDir/contrib/resource_util" as File
-project(':opencensus-contrib-spring').projectDir = "$rootDir/contrib/spring" as File
-project(':opencensus-contrib-spring-sleuth-v1x').projectDir =
-        "$rootDir/contrib/spring_sleuth_v1x" as File
-project(':opencensus-contrib-spring-starter').projectDir = "$rootDir/contrib/spring_starter" as File
-project(':opencensus-contrib-zpages').projectDir = "$rootDir/contrib/zpages" as File
-project(':opencensus-exporter-metrics-ocagent').projectDir =
-        "$rootDir/exporters/metrics/ocagent" as File
-project(':opencensus-exporter-metrics-util').projectDir =
-        "$rootDir/exporters/metrics/util" as File
-project(':opencensus-exporter-stats-signalfx').projectDir =
-        "$rootDir/exporters/stats/signalfx" as File
-project(':opencensus-exporter-stats-stackdriver').projectDir =
-        "$rootDir/exporters/stats/stackdriver" as File
-project(':opencensus-exporter-stats-prometheus').projectDir =
-        "$rootDir/exporters/stats/prometheus" as File
-project(':opencensus-exporter-trace-elasticsearch').projectDir =
-        "$rootDir/exporters/trace/elasticsearch" as File
-project(':opencensus-exporter-trace-datadog').projectDir =
-        "$rootDir/exporters/trace/datadog" as File
-project(':opencensus-exporter-trace-instana').projectDir =
-        "$rootDir/exporters/trace/instana" as File
-project(':opencensus-exporter-trace-logging').projectDir =
-        "$rootDir/exporters/trace/logging" as File
-project(':opencensus-exporter-trace-ocagent').projectDir =
-        "$rootDir/exporters/trace/ocagent" as File
-project(':opencensus-exporter-trace-stackdriver').projectDir =
-        "$rootDir/exporters/trace/stackdriver" as File
-project(':opencensus-exporter-trace-zipkin').projectDir = "$rootDir/exporters/trace/zipkin" as File
-project(':opencensus-exporter-trace-jaeger').projectDir = "$rootDir/exporters/trace/jaeger" as File
-project(':opencensus-exporter-trace-util').projectDir = "$rootDir/exporters/trace/util" as File
--- a/api/build.gradle
+++ b/api/build.gradle
@@ -4,9 +4,8 @@
     compile libraries.grpc_context
 
     compileOnly libraries.auto_value
+    compileOnly libraries.auto_value_a
 
-    signature "org.codehaus.mojo.signature:java17:1.0@signature"
-    signature "net.sf.androidscents.signature:android-api-level-14:4.0_r4@signature"
 }
 
 javadoc {
--- a/contrib/grpc_metrics/build.gradle
+++ b/contrib/grpc_metrics/build.gradle
@@ -3,14 +3,12 @@
 apply plugin: 'java'
 
 [compileJava, compileTestJava].each() {
-    it.sourceCompatibility = 1.6
-    it.targetCompatibility = 1.6
+    it.sourceCompatibility = 1.7
+    it.targetCompatibility = 1.7
 }
 
 dependencies {
     compile project(':opencensus-api'),
             libraries.guava
 
-    signature "org.codehaus.mojo.signature:java17:1.0@signature"
-    signature "net.sf.androidscents.signature:android-api-level-14:4.0_r4@signature"
 }
--- a/contrib/http_util/build.gradle
+++ b/contrib/http_util/build.gradle
@@ -3,14 +3,11 @@
 apply plugin: 'java'
 
 [compileJava, compileTestJava].each() {
-    it.sourceCompatibility = 1.6
-    it.targetCompatibility = 1.6
+    it.sourceCompatibility = 1.7
+    it.targetCompatibility = 1.7
 }
 
 dependencies {
     compile project(':opencensus-api'),
             libraries.guava
-
-    signature "org.codehaus.mojo.signature:java17:1.0@signature"
-    signature "net.sf.androidscents.signature:android-api-level-14:4.0_r4@signature"
 }
